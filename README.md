# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/gazebo_user_guide

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-archived-gh-pages/#!/osrf/gazebo_user_guide

## Until May 31st 2020, the mercurial repository can be found at

https://bitbucket.org/osrf-migrated/gazebo_user_guide
